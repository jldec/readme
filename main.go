package main

import (
	"fmt"
	"net/http"
)

func main() {
	var count uint64 = 0

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		// this is racey - see https://jldec.me/getting-started-with-go-part-3-goroutines-and-channels
		count++

		fmt.Fprintln(w, count)
		// fmt.Println(r.URL, count)
	})

	fmt.Println("Go listening on port 3000")
	http.ListenAndServe(":3000", nil)
}
